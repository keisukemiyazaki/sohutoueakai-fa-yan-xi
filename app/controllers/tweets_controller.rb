class TweetsController < ApplicationController
  def index
    @tweets = Tweet.all
  end

  def show
    @tweet = Tweet.find(params[:id])
  end

  def new
    @tweet = Tweet.new
  end

  def create
    messeage = params[:tweet][:messeage]
    tdate = Time.current
    @tweet = Tweet.new(messeage: messeage, tdate: tdate)
    if @tweet.save
      flash[:notice] = '投稿しました'
      redirect_to '/'
    else
      render 'new'
    end
  end

  def destroy
    tweet = Tweet.find(params[:id])
    tweet.destroy
    redirect_to '/'
  end

  def edit
    @tweet = Tweet.find(params[:id])
  end

  def update
    @tweet = Tweet.find(params[:id])
    messeage = params[:tweet][:messeage]
    @tweet.update(messeage: messeage)
    redirect_to '/'
  end
end
